# -*- coding: utf-8 -*-
import typing
from random import SystemRandom


class RandomDevice(object):
    """
    Device that generates random numbers between ``self.num_from`` and ``self.num_to``.
    """
    def __init__(self):
        self.results = []
        """
        Array to hold results.
        """
        self.num_from = None
        self.num_to = None
        self.number_of_points_per_iteration = 2

        self.random = None
        """
        Instance of SystemRandom
        """

    def apply_settings(self, settings: typing.Mapping[str, typing.Any]):
        self.num_from = settings['num_from']
        self.num_to = settings['num_to']
        self.number_of_points_per_iteration = settings.get('num_points', 2)

    def start(self):
        self.results = []
        self.random = SystemRandom()

    def stop(self):
        self.random = None
        self.results = []

    def pop_results(self):
        """

        :return:
        """
        self.get_results_from_fictional_hardware()
        try:
            return self.results
        finally:
            self.results = []

    def get_results_from_fictional_hardware(self):
        """
        In typical device this would gather results from equipment, typically this would
        be done in a background thread, or in similar way, Then these results would be stored
        on device, and returned on ``pop_results``.
        """
        if self.random is not None:
            self.results.append({
                'num': [
                    self.random.randint(self.num_from, self.num_to)
                    for __ in range(self.number_of_points_per_iteration)
                ]
            })
