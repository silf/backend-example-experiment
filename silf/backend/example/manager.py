import configparser
import typing

from silf.backend.commons.api.translations.trans import _

from silf.backend.commons.new.experiment_mode import ExperimentMode
from silf.backend.commons.simple_server.simple_server import MultiModeManager

from silf.backend.example.modes import HistogramMode, LiveAverageMode
from .modes import DisplayRandomNumbersMode, AverageMode


class RandomExperimentManager(MultiModeManager):

    @classmethod
    def get_mode_managers(
        cls, config_parser: configparser.ConfigParser
    ):
        return [
            DisplayRandomNumbersMode,
            AverageMode,
            HistogramMode,
            LiveAverageMode
        ]

    def post_power_up_diagnostics(self, *args, **kwargs):
        pass

    def pre_power_up_diagnostics(self, *args, **kwargs):
        pass
