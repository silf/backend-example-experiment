import configparser

from silf.backend.commons.api.stanza_content import (
    ControlSuite,
    OutputFieldSuite
)
from silf.backend.commons.api.stanza_content import ChartField, OutputField, SettingSuite
from silf.backend.commons.api.stanza_content.control import NumberControl
from silf.backend.commons.experiment._experiment_manager import ExperimentCallback
from silf.backend.commons.simple_server.simple_server import ExperimentMode, ResultManager

from silf.backend.commons.api.translations.trans import _

from silf.backend.example.result_creators import ChartAverageResultCreator, AverageResultCreator, \
    HistogramCreator
from .result_creators import CountItemsResultCreator, PointChartCreator
from .device import RandomDevice


class DisplayRandomNumbersMode(ExperimentMode):

    def __init__(self, config_parser: configparser.ConfigParser,
                 experiment_callback: ExperimentCallback):
        super().__init__(config_parser, experiment_callback)
        self.device = None
        self.__started = False
        self.result_manager = ResultManager(
            callback=self.experiment_callback,
            creators=[
                CountItemsResultCreator(result_name="count", read_from="num"),
                PointChartCreator(result_name="chart", read_from="num"),
            ])

    @classmethod
    def get_mode_name(cls) -> str:
        return "display-numbers"

    @classmethod
    def get_mode_label(cls):
        return _("Random numbers")

    @classmethod
    def get_description(cls) -> str:
        return _("Just display random numbers")

    @classmethod
    def get_input_fields(cls) -> ControlSuite:
        return ControlSuite(
            NumberControl(
                "num_from", _("Start number"), min_value=0, max_value=50, step=1, default_value=1
            ),
            NumberControl(
                "num_to", _("End number"), min_value=0, max_value=50, step=1, default_value=10
            ),
        )

    @classmethod
    def get_output_fields(cls) -> OutputFieldSuite:
        return OutputFieldSuite(
            chart=ChartField(
                html_class="chart", name=["chart"],
                label=_("Result Chart"),
                axis_x_label=_("Point index"), axis_y_label=_("Random number")),
            count=OutputField(
                html_class="integer-indicator", name=["count"], label=_("Number of points"))
        )

    def power_up(self):
        self.device = RandomDevice()

    def power_down(self):
        self.device = None

    def loop_iteration(self):
        if self.__started:
            results = self.device.pop_results()
            self.result_manager.push_results(results)

    @classmethod
    def get_series_name(cls, settings: dict) -> str:
        return _("Series {num_from}:{num_to}").format(**settings)

    def stop(self):
        self.__started = False
        self.result_manager.clear()

    def apply_settings(self, settings: SettingSuite) -> None:
        super().apply_settings(settings)
        self.device.apply_settings(self.settings)

    def apply_settings_live(self, settings: SettingSuite):
        super().apply_settings_live(settings)
        self.device.apply_settings(self.settings)

    def start(self):
        self.__started = True
        self.device.start()


class AverageMode(DisplayRandomNumbersMode):

    @classmethod
    def get_output_fields(cls) -> OutputFieldSuite:
        return OutputFieldSuite(
            chart=ChartField(
                html_class="chart", name=["chart"], label=_("Result Chart"),
                axis_x_label=_("Number of points"), axis_y_label=_("Average of all points")),
            count=OutputField(
                html_class="integer-indicator", name=["count"], label=_("Number of points")),
            average=OutputField(
                html_class="integer-indicator", name=["average"], label=_("Point average"))
        )

    def __init__(self, config_parser: configparser.ConfigParser,
                 experiment_callback: ExperimentCallback):
        super().__init__(config_parser, experiment_callback)
        self.result_manager = ResultManager(
            callback=self.experiment_callback,
            creators=[
                CountItemsResultCreator(result_name="count", read_from="num"),
                AverageResultCreator(result_name="average", read_from="num"),
                ChartAverageResultCreator(result_name="chart", read_from="num"),
            ])

    @classmethod
    def get_description(cls) -> str:
        return _("Displays running average of random numbers")

    @classmethod
    def get_mode_name(cls) -> str:
        return "average"

    @classmethod
    def get_mode_label(cls):
        return _("Running average")


class LiveAverageMode(AverageMode):
    @classmethod
    def get_input_fields(cls) -> ControlSuite:
        return ControlSuite(
            NumberControl(
                "num_from", _("Start number"), min_value=0, max_value=50, step=1, default_value=1,
                live=True
            ),
            NumberControl(
                "num_to", _("End number"), min_value=0, max_value=50, step=1, default_value=10,
                live=True
            ),
        )

    @classmethod
    def get_description(cls) -> str:
        return _("Displays running average of random numbers (with live controls)")

    @classmethod
    def get_mode_name(cls) -> str:
        return "live-average"

    @classmethod
    def get_mode_label(cls):
        return _("Live running average")

class HistogramMode(DisplayRandomNumbersMode):

    @classmethod
    def get_input_fields(cls) -> ControlSuite:
        return ControlSuite(
            NumberControl(
                "num_from", _("Start number"), min_value=0, max_value=50, step=1, default_value=1
            ),
            NumberControl(
                "num_to", _("End number"), min_value=0, max_value=50, step=1, default_value=10
            ),
            NumberControl(
                "sum_numbers", _("Numbers to sum"), min_value=0, max_value=50, step=1, default_value=8
            ),
        )

    @classmethod
    def get_output_fields(cls) -> OutputFieldSuite:
        return OutputFieldSuite(
            chart=ChartField(
                html_class="chart", name=["chart"], label=_("Result Chart"),
                axis_x_label=_("Sum of random numbers"), axis_y_label=_("Number of instances")),
            count=OutputField(
                html_class="integer-indicator", name=["count"], label=_("Number of points")),
            average=OutputField(
                html_class="integer-indicator", name=["average"], label=_("Point average"))
        )

    def __init__(self, config_parser: configparser.ConfigParser,
                 experiment_callback: ExperimentCallback):
        super().__init__(config_parser, experiment_callback)

    def apply_settings(self, settings: SettingSuite) -> None:
        super().apply_settings(settings)
        self.device.number_of_points_per_iteration = 24

    @classmethod
    def get_description(cls) -> str:
        return _("Displays histogram of sum of random numbers.")

    @classmethod
    def get_mode_label(cls):
        return _("Gauss histogram")

    @classmethod
    def get_mode_name(cls) -> str:
        return "histogram"

    def start(self):
        numbers_to_sum = self.settings['sum_numbers']
        num_from = self.settings['num_from']
        num_to = self.settings['num_to']
        right_bound = num_to * numbers_to_sum
        left_bound = num_from * numbers_to_sum
        self.result_manager = ResultManager(
            callback=self.experiment_callback,
            creators=[
                CountItemsResultCreator(result_name="count", read_from="num"),
                HistogramCreator(
                    result_name="chart",
                    read_from="num",
                    number_to_sum=numbers_to_sum,
                    hist_bounds=(left_bound, right_bound)
                ),
            ])
        super().start()


