import typing

from silf.backend.commons.api.stanza_content import Result
from silf.backend.commons.device_manager import ResultCreator


class CountItemsResultCreator(ResultCreator):
    """"
    A result creators that counts numbers in sequence.

    >>> creator = CountItemsResultCreator("foo")

    After construction instances have no results

    >>> creator.has_results
    False

    After we add some results we it will return count of the results.

    >>> creator.aggregate_results({"foo": [1, 2, 3]})
    >>> creator.has_results
    True
    >>> creator.pop_results()
    <Result pragma=transient value=3 >
    >>> creator.pop_results()
    <Result pragma=transient value=3 >


    >>> creator.aggregate_results({"foo": [1, 2, 3]})
    >>> creator.pop_results()
    <Result pragma=transient value=6 >

    After call to clear results are not avilable.

    >>> creator.clear()
    >>> creator.has_results
    False

    It also handles case when it can't find any results.

    >>> creator.aggregate_results({})
    >>> creator.has_results
    False
    >>> creator.aggregate_results({"foo": None})
    >>> creator.has_results
    False
    """

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('pragma', 'transient')
        super().__init__(*args, **kwargs)
        self.count = None

    def clear(self):
        self.count = None

    def aggregate_results(self, results):
        value = results.get(self.read_from, None)
        if value is None:
            return
        if self.count is None:
            self.count = 0
        self.count += len(value)

    def pop_results(self):
        return Result(
            pragma=self.pragma,
            value=self.count
        )

    @property
    def has_results(self):
        return self.count is not None


class PointChartCreator(ResultCreator):
    """
    >>> creator = PointChartCreator(result_name="foo")
    >>> creator.has_results()
    False

    >>> creator.aggregate_results({"foo": [1, 2, 3, 4]})
    >>> creator.has_results()
    True
    >>> creator.pop_results()
    <Result pragma=append value=[[0, 1], [1, 2], [2, 3], [3, 4]] >
    >>> creator.has_results()
    False

    >>> creator.aggregate_results({"foo": [1, 2, 3, 4]})
    >>> creator.pop_results()
    <Result pragma=append value=[[4, 1], [5, 2], [6, 3], [7, 4]] >

    """

    def __init__(self, *args, **kwargs):
        kwargs.setdefault("pragma", "append")
        super().__init__(*args, **kwargs)
        self.points = []
        self.point_count = 0

    def clear(self):
        self.points = []
        self.point_count = 0

    def has_results(self):
        return len(self.points) > 0

    def aggregate_results(self, results):
        result = results.get(self.read_from, [])
        self.points.extend([
            [self.point_count + idx, val]
            for idx, val in enumerate(result)
        ])
        self.point_count += len(result)

    def pop_results(self) -> Result:
        try:
            return Result(
                value=self.points
            )
        finally:
            self.points = []


class AverageResultCreator(ResultCreator):
    """
    >>> creator = AverageResultCreator(result_name="foo")
    >>> creator.has_results
    False

    >>> creator.aggregate_results({"foo": [1, 2, 3]})
    >>> creator.has_results
    True

    >>> creator.pop_results()
    <Result pragma=transient value=2.0 >
    """

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('pragma', 'transient')
        super().__init__(*args, **kwargs)
        self.count = None
        self.sum = None

    def clear(self):
        self.count = None
        self.sum = None

    def aggregate_results(self, results):
        value = results.get(self.read_from, None)
        if value is None:
            return
        if self.count is None:
            self.count = 0
            self.sum = 0
        self.count += len(value)
        self.sum += sum(value, 0)

    def pop_results(self):
        return Result(
            pragma=self.pragma,
            value=self.sum / self.count
        )

    @property
    def has_results(self):
        return self.count is not None


class ChartAverageResultCreator(ResultCreator):
    """

    >>> creator = ChartAverageResultCreator("foo")


    >>> creator.has_results()
    False

    >>> creator.aggregate_results({"foo": [1, 2, 3, 4]})

    >>> creator.has_results()
    True

    >>> creator.pop_results()
    <Result pragma=append value=[[1, 1.0], [2, 1.5], [3, 2.0], [4, 2.5]] >


    >>> creator.has_results()
    False

    >>> creator.aggregate_results({"foo": [1, 2]})

    >>> creator.pop_results()
    <Result pragma=append value=[[5, 2.2], [6, 2.1666666666666665]] >

    """
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("pragma", "append")
        super().__init__(*args, **kwargs)
        self.points = None
        self.point_count = None
        self.sum = None

    def clear(self):
        self.points = None
        self.point_count = None
        self.sum = None

    def has_results(self):
        return self.points is not None and len(self.points) > 0

    def aggregate_results(self, results):
        result = results.get(self.read_from, [])
        if result is None:
            return
        if self.point_count is None:
            self.point_count = 0
            self.sum = 0
            self.points = []

        for point in result:
            self.sum += point
            self.point_count += 1
            self.points.append([self.point_count, self.sum / self.point_count])

    def pop_results(self) -> Result:
        try:
            return Result(
                value=self.points
            )
        finally:
            self.points = []


class HistogramCreator(ResultCreator):

    def __init__(
        self,
        result_name, *,
        read_from=None,
        number_to_sum=5,
        hist_bounds=typing.Tuple[int, int]
    ):
        super().__init__(result_name, 'replace', read_from=read_from)
        self.histogram = None
        self.current_results = None
        self.number_to_sum = int(number_to_sum)
        self.hist_bounds = list(map(int, hist_bounds))

    def pop_results(self) -> Result:
        return Result(
            value=[
                [position, count]
                for position, count
                in zip(range(self.hist_bounds[0], self.hist_bounds[1]), self.histogram)
            ],
            pragma=self.pragma
        )

    @property
    def has_results(self):
        return self.histogram is not None

    def aggregate_results(self, results):
        result = results.get(self.read_from)
        if result is None:
            return
        if self.histogram is None:
            self.histogram = [0] * int((self.hist_bounds[1] - self.hist_bounds[0]) - 1)
            self.current_results = []
        self.current_results.extend(result)
        while len(self.current_results) > self.number_to_sum:
            summed = sum(self.current_results[:self.number_to_sum], 0)
            self.current_results = self.current_results[self.number_to_sum:]
            summed -= self.hist_bounds[0]
            self.histogram[summed] += 1

    def clear(self):
        self.histogram = None
        self.current_results = None
