#!/bin/sh

scripts/j2cli.py docker.ini > docker-ready.ini

cat docker-ready.ini

while ! nc -q 1 $TIGASE_HOST 5222 </dev/null; do
    echo "Waiting for tigase to listen on port 5222 ..."
    sleep 3;
done

set -e

export PYTHONPATH=/experiment

# Quick work-around for SILF-1283. Please remove once it is done.
sleep 30

expmain.py docker-ready.ini
