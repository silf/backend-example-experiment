��          �               L     M  ,   c  *   �  
   �     �     �     �               &     4     @     N     ]     j     z     �     �  �  �      �     �  3   �     �          "     =     R     c          �     �     �     �     �     �     �     �   Average of all points Displays histogram of sum of random numbers. Displays running average of random numbers End number Gauss histogram Just display random numbers Number of instances Number of points Numbers to sum Point average Point index Random number Random numbers Result Chart Running average Series {num_from}:{num_to} Start number Sum of random numbers Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2016-11-08 16:32+0100
PO-Revision-Date: 2016-11-08 14:02+0100
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: pl
Language-Team: pl <LL@li.org>
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.3.4
 Średnia dla wszystkich punktów Histogram sum liczb losowych Wyświetla biegnącą średnią z losoanych liczb Końcowa liczba Centralne Twierdzenei Graniczne Wyświetla losowane liczby Ilość wystąpień Ilość punktów Ilość liczb do zsumowania Średnia Numer punktu Wylosowana liczba Losowe liczby Wyniki Biegnąca średnia Seria {num_from}:{num_to} Początkowa liczba Suma losowych liczb 